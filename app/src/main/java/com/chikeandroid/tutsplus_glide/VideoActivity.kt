package com.chikeandroid.tutsplus_glide

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.VideoView

import com.bumptech.glide.Glide

class VideoActivity : AppCompatActivity() {

    private var videoView: VideoView? = null
    internal var filePath: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_video)

        filePath = intent.getStringExtra("videoPath")

        videoView = findViewById(R.id.video) as VideoView

        videoView!!.setVideoPath(filePath)

        videoView!!.start()

    }
}
